﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;

public class Test2 : MonoBehaviour
{
    public List<Grid> grids;
    public Grid test;

    private void Start()
    {
        //test.FindWords();
        GetFile();

    }

    public void GetFile()
    {
        int lineCount = 1;
        int currentNumber = 0;
        int currentCaseNumberLine = 0;
        int nextcaseNumberLine = 2;
        int writingCharListIndex = 0;

        string line;
        StreamReader file = new StreamReader(Application.persistentDataPath + "/input.in");
        while ((line = file.ReadLine()) != null)
        {
            if (lineCount == 1)
            {
                int useCase = Int32.Parse(line);

                for (int x = 0; x < useCase; x++)
                {
                    grids.Add(new Grid());
                }
            }
            else if (lineCount == nextcaseNumberLine)
            {
                nextcaseNumberLine = nextcaseNumberLine + (Int32.Parse(line) + 3);
                currentCaseNumberLine = lineCount;
                currentNumber++;

                grids[currentNumber - 1].grid = new List<Col>();

                int howManyRows = Int32.Parse(line);
                for (int x = 0; x < howManyRows; x++)
                {

                    grids[currentNumber - 1].grid.Add(new Col(1));

                }
            }

            if (lineCount < nextcaseNumberLine && lineCount > currentCaseNumberLine && currentCaseNumberLine != 0)
            {
                if (lineCount == currentCaseNumberLine + 1)
                {

                    int howManyCol = Int32.Parse(line);/*
                    foreach(Col gridCol in grids[currentNumber - 1].grid)
                    {
                        for(int x = 0; x < howManyCol;x++)
                        {
                            gridCol.col.Add('a');
                        }
                    }*/
                    writingCharListIndex = 0;
                }
                else if (lineCount != nextcaseNumberLine - 1)
                {
                    char[] content = line.ToCharArray();
                    foreach (char c in content)
                    {
                        grids[currentNumber - 1].grid[writingCharListIndex].col.Add(c);
                    }
                    writingCharListIndex++;
                }
                else
                {
                    grids[currentNumber - 1].wordToFind = line;
                }
            }

            lineCount++;
        }

        file.Close();

        FindSolution();
    }

    private void FindSolution()
    {
        string result = "";
        string[] lines = new string[grids.Count];
        for (int x = 0; x < grids.Count; x++)
        {
            grids[x].FindWords();
            lines[x] = "Case " + (x + 1) + ": " + grids[x].totalCount;
            //result += "Case " + (x + 1) + ": " + grids[x].totalCount + "  ";
        }
        Debug.Log(result);

        StreamWriter writer = new StreamWriter(Application.persistentDataPath + "/output.out");
        foreach(string line in lines)
        {
            writer.WriteLine(line);
        }
        writer.Close();

    }

}

[System.Serializable]
public class Grid
{

    public List<Col> grid;
    public string wordToFind;
    public int totalCount;
    private char[] chars;
    private int currentCharsIndex;
    private int currentFindingRow;
    private int currentFindingCol;

    public Grid()
    {
        grid = new List<Col>();

    }

    public void FindWords()
    {
        chars = wordToFind.ToCharArray();

        for (int row = 0; row < grid.Count; row++)
        {
            for (int col = 0; col < grid[row].col.Count; col++)
            {
                if (grid[row].col[col] == chars[0])
                {
                    //get upper left
                    currentCharsIndex = 0;
                    currentFindingRow = row - 1;
                    currentFindingCol = col - 1;
                    FindTheRest(Orientation.UPPERLEFT);

                    //get upper
                    currentCharsIndex = 0;
                    currentFindingRow = row - 1;
                    currentFindingCol = col;
                    FindTheRest(Orientation.UPPER);

                    //get upper right
                    currentCharsIndex = 0;
                    currentFindingRow = row - 1;
                    currentFindingCol = col + 1;
                    FindTheRest(Orientation.UPPERRIGHT);

                    //get left
                    currentCharsIndex = 0;
                    currentFindingRow = row;
                    currentFindingCol = col - 1;
                    FindTheRest(Orientation.LEFT);

                    //get right
                    currentCharsIndex = 0;
                    currentFindingRow = row;
                    currentFindingCol = col + 1;
                    FindTheRest(Orientation.RIGHT);

                    //get lower left
                    currentCharsIndex = 0;
                    currentFindingRow = row + 1;
                    currentFindingCol = col - 1;
                    FindTheRest(Orientation.LOWERLEFT);

                    //get lower
                    currentCharsIndex = 0;
                    currentFindingRow = row + 1;
                    currentFindingCol = col;
                    FindTheRest(Orientation.LOWER);

                    //get lower right
                    currentCharsIndex = 0;
                    currentFindingRow = row + 1;
                    currentFindingCol = col + 1;
                    FindTheRest(Orientation.LOWERRIGHT);


                }

            }
        }

    }

    public void FindTheRest(Orientation orientation)
    {
        switch (orientation)
        {
            case Orientation.UPPERLEFT:
                currentFindingCol--;
                currentFindingRow--;
                currentCharsIndex++;
                char upperLeft = FindNeighbour(currentFindingRow, currentFindingCol);
                try
                {
                    if (upperLeft != ' ' && upperLeft == chars[currentCharsIndex + 1] && currentCharsIndex + 1 < chars.Length)
                    {
                        if (currentCharsIndex + 1 == chars.Length - 1)
                        {
                            totalCount++;
                        }
                        else
                        {
                            FindTheRest(orientation);
                        }

                    }
                }
                catch
                {

                }
                break;
            case Orientation.UPPER:
                currentFindingRow--;
                currentCharsIndex++;
                try
                {
                    char upper = FindNeighbour(currentFindingRow, currentFindingCol);
                    if (upper != ' ' && upper == chars[currentCharsIndex + 1] && currentCharsIndex + 1 < chars.Length)
                    {
                        if (currentCharsIndex + 1 == chars.Length - 1)
                        {
                            totalCount++;
                        }
                        else
                        {
                            FindTheRest(orientation);
                        }

                    }
                }
                catch
                {

                }
                break;

            case Orientation.UPPERRIGHT:
                currentFindingCol++;
                currentFindingRow--;
                currentCharsIndex++;
                char upperRight = FindNeighbour(currentFindingRow, currentFindingCol);
                try
                {
                    if (upperRight != ' ' && upperRight == chars[currentCharsIndex + 1] && currentCharsIndex + 1 < chars.Length)
                    {
                        if (currentCharsIndex + 1 == chars.Length - 1)
                        {
                            totalCount++;
                        }
                        else
                        {
                            FindTheRest(orientation);
                        }

                    }
                }
                catch
                {

                }
                break;
            case Orientation.LEFT:
                currentFindingCol--;
                currentCharsIndex = currentCharsIndex + 1;
                char left = FindNeighbour(currentFindingRow, currentFindingCol);
                try
                {
                    if (left != ' ' && left == chars[currentCharsIndex + 1] && currentCharsIndex + 1 < chars.Length)
                    {
                        if (currentCharsIndex + 1 == chars.Length - 1)
                        {
                            totalCount++;
                        }
                        else
                        {
                            FindTheRest(orientation);
                        }

                    }
                }
                catch
                {

                }
                break;
            case Orientation.RIGHT:
                currentFindingCol = currentFindingCol + 1;
                currentCharsIndex = currentCharsIndex + 1;
                char right = FindNeighbour(currentFindingRow, currentFindingCol);
                try
                {
                    if (right != ' ' && right == chars[currentCharsIndex + 1] && currentCharsIndex + 1 < chars.Length)
                    {
                        if (currentCharsIndex + 1 == chars.Length - 1)
                        {
                            totalCount++;
                        }
                        else
                        {
                            FindTheRest(orientation);
                        }

                    }
                }
                catch
                {

                }
                break;
            case Orientation.LOWERLEFT:
                currentFindingCol--;
                currentFindingRow++;
                currentCharsIndex++;
                char lowerLeft = FindNeighbour(currentFindingRow, currentFindingCol);
                try
                {
                    if (lowerLeft != ' ' && lowerLeft == chars[currentCharsIndex + 1] && currentCharsIndex + 1 < chars.Length)
                    {
                        if (currentCharsIndex + 1 == chars.Length - 1)
                        {
                            totalCount++;
                        }
                        else
                        {
                            FindTheRest(orientation);
                        }

                    }
                }
                catch
                {

                }
                break;
            case Orientation.LOWER:
                currentFindingRow++;
                currentCharsIndex++;
                char lower = FindNeighbour(currentFindingRow, currentFindingCol);
                try
                {
                    if (lower != ' ' && lower == chars[currentCharsIndex + 1] && currentCharsIndex + 1 < chars.Length)
                    {
                        if (currentCharsIndex + 1 == chars.Length - 1)
                        {
                            totalCount++;
                        }
                        else
                        {
                            FindTheRest(orientation);
                        }

                    }
                }
                catch
                {

                }
                break;
            case Orientation.LOWERRIGHT:
                currentFindingCol++;
                currentFindingRow++;
                currentCharsIndex++;
                char lowerRight = FindNeighbour(currentFindingRow, currentFindingCol);
                try
                {
                    if (lowerRight != ' ' && lowerRight == chars[currentCharsIndex + 1] && currentCharsIndex + 1 < chars.Length)
                    {
                        if (currentCharsIndex + 1 == chars.Length - 1)
                        {
                            totalCount++;
                        }
                        else
                        {
                            FindTheRest(orientation);
                        }

                    }
                }
                catch
                {

                }
                break;
        }


    }

    public char FindNeighbour(int row, int col)
    {
        try
        {
            return grid[row].col[col];
        }
        catch
        {
            return ' ';
        }
    }


}

public enum Orientation
{
    LEFT, RIGHT, UPPERLEFT, UPPER, UPPERRIGHT, LOWERLEFT, LOWER, LOWERRIGHT
}

[System.Serializable]
public struct Col
{
    public List<char> col;

    public Col(int id)
    {
        col = new List<char>();
    }

}