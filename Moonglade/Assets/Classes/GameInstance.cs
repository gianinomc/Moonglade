﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class GameInstance : MonoBehaviour {

    private static GameInstance instance = null;
    [SerializeField]
    private GameState gameState;
    public Modal modal;
    
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            gameState = GameState.STARTUP;
            modal.HideModal();
            GPlayServLogin();
        }

        if(SceneManager.GetActiveScene().name == "tutorial" || SceneManager.GetActiveScene().name == "level")
        {
            gameState = GameState.PLAYING;
        }

        Application.targetFrameRate = 60;
        DontDestroyOnLoad(gameObject);
    }
    
    public static void GPlayServLogin()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        PlayGamesPlatform.Instance.Authenticate(GPlayServSignInCallback, true);
    }

    public static void GPlayServSignInCallback(bool success)
    {
        if (success)
        {
            Debug.Log(Social.localUser.userName + " Signed in! gplayserv");
        }
        else
        {
            Debug.Log("Sign-in failed... gplayserv");
        }
    }

    private void Start()
    {
        if(gameState == GameState.STARTUP)
        {
            MenuGameUI gameUI = (MenuGameUI)GetGameMode().GetGameUI();

            bool musicOn = !Convert.ToBoolean(PlayerPrefs.GetInt("mute_sfx"));
            bool sfxOn = !Convert.ToBoolean(PlayerPrefs.GetInt("mute_music"));

            GameAudio.ToggleSFX(musicOn);
            GameAudio.ToggleMusic(sfxOn);
            gameUI.ShowSplashScreen(CheckUserTutorial);
        }
    }

    public void CheckUserTutorial()
    {
        bool firstStartup = Convert.ToBoolean(PlayerPrefs.GetInt("tutorial_complete"));
        if (!firstStartup)
        {
            modal.ShowModal("Do you want to play tutorial?", ModalTutorialYes, ModalTutorialNo);
        }
    }

    public void ModalTutorialYes()
    {
        MenuGameMode gameMode = (MenuGameMode)GetGameMode();
        gameMode.PlayTutorial();
        PlayerPrefs.SetInt("tutorial_complete", 1);
    }

    public void ModalTutorialNo()
    {
        PlayerPrefs.SetInt("tutorial_complete", 1);
    }

    public static GameMode GetGameMode()
    {
        GameMode gameMode = GameObject.Find("GameMode").GetComponent<GameMode>();
        return gameMode;
    }

    public static void SetGameState(GameState gameState)
    {
        instance.gameState = gameState;
    }

    public static GameState GetGameState()
    {
        return instance.gameState;
    }

    public static void LoadLevel(string levelName)
    {
        GameAds.HideBanner();
        SceneManager.LoadScene(levelName);
    }

    public static void ShowLeaderBoards()
    {
        if (PlayGamesPlatform.Instance.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_classic);
        }
        else
        {
            Debug.Log("gplayserv Cannot show leaderboard: not authenticated");
        }
    }
}

[System.Serializable]
public enum GameState
{
    MENU, PLAYING, PAUSE, STARTUP, SETTINGS
}