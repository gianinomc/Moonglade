﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SliderObs : Obstacle
{

    public GameObject[] toSlide;
    public bool toLeft;
    public float speed;
    public float maximumEdge;

    [SerializeField]
    private List<GameObject> sortedSlide;
    private Vector3 firstPosition;
    private Vector3 lastPosition;
    private List<Vector3> initialSlidePosition;


    protected override void Awake()
    {
        base.Awake();

    }

    protected override void Start()
    {
        base.Start();
        sortedSlide = toSlide.OrderBy(o => o.transform.localPosition.x).ToList();
        firstPosition = sortedSlide[0].transform.localPosition;
        lastPosition = sortedSlide[sortedSlide.Count - 1].transform.localPosition;

        initialSlidePosition = new List<Vector3>();
        foreach (GameObject slide in sortedSlide)
        {
            initialSlidePosition.Add(slide.transform.localPosition);
        }

        //SetupObstacle();
    }

    public override void SetupObstacle()
    {
        base.SetupObstacle();

        for (int x = 0; x < sortedSlide.Count; x++)
        {
            sortedSlide[x].transform.localPosition = initialSlidePosition[x];
        }

    }

    protected override void Update()
    {
        base.Update();
        foreach (GameObject go in sortedSlide)
        {
            Vector3 moveDirection = Vector3.left;
            if (!toLeft)
            {
                moveDirection = -Vector3.left;

                if (go.transform.localPosition.x > lastPosition.x + go.GetComponent<SpriteRenderer>().bounds.size.x)
                {
                    Vector3 newPos = firstPosition;
                    newPos.x = firstPosition.x;
                    go.transform.localPosition = newPos;
                }

            }
            else
            {
                if (go.transform.localPosition.x < firstPosition.x - go.GetComponent<SpriteRenderer>().bounds.size.x)
                {
                    Vector3 newPos = lastPosition;
                    newPos.x = lastPosition.x;
                    go.transform.localPosition = newPos;
                }
            }
            go.transform.Translate(moveDirection * Time.deltaTime * speed);
        }
    }
}
