﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorObs : Obstacle
{
    public float rotationSpeed;
    public float maximumRandomSpeed;
    public GameObject rotator;
    private Vector3 firstRotation;

    protected override void Awake()
    {
        base.Awake();
        firstRotation = rotator.transform.eulerAngles;
    }

    protected override void Update()
    {
        base.Update();
        rotator.transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
    }

    public void RandomRotationSpeed()
    {
        float randomRange = Random.Range(-maximumRandomSpeed, maximumRandomSpeed);
        if (randomRange < 30f && randomRange > -30f)
        {
            RandomRotationSpeed();
        }

        rotationSpeed = randomRange;
    }

    public override void SetupObstacle()
    {
        base.SetupObstacle();
        rotator.transform.eulerAngles = firstRotation;

        if (randomSpeed)
        {
            RandomRotationSpeed();
        }
    }

}
