﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class MenuGameUI : GameUI
{

    public Image splashScreen;
    public Image logoPublisher;
    public Text titlePublisher;

    public RectTransform settingPanel;
    public UIImageToggle musicToggle;
    public UIImageToggle sfxToggle;
    public List<Rotator> rotators;

    private Vector2 settingPanelFirstPosition;

    public delegate void AfterSplashScreen();

    protected override void Awake()
    {
        base.Awake();

        settingPanelFirstPosition = settingPanel.anchoredPosition;
        settingPanelFirstPosition.y = Screen.height * 2;
        settingPanel.anchoredPosition = settingPanelFirstPosition;
    }

    public void ShowSplashScreen(AfterSplashScreen after)
    {
        StartCoroutine(SplashScreen(after));   
    }

    private IEnumerator SplashScreen(AfterSplashScreen after)
    {
        splashScreen.gameObject.SetActive(true);
        Sequence splashScreenSeq = DOTween.Sequence();
        splashScreenSeq.Append(logoPublisher.DOFade(1f, 1f));
        splashScreenSeq.Insert(0f, titlePublisher.DOFade(1f, 1f));
        splashScreenSeq.Insert(3f, splashScreen.rectTransform.DOAnchorPosY(Screen.height * 2, 0.5f, true));
        splashScreenSeq.Play();

        yield return splashScreenSeq.WaitForCompletion();

        GameInstance.SetGameState(GameState.MENU);
        after();
        
    }

    private void ActivateRotator()
    {
        foreach(Rotator rot in rotators)
        {
            rot.gameObject.SetActive(true);
        }
    }

    private void DeactiveRotator()
    {
        foreach (Rotator rot in rotators)
        {
            rot.gameObject.SetActive(false);
        }
    }

    public void OpenSettings()
    {
        GameInstance.SetGameState(GameState.SETTINGS);
        DeactiveRotator();

        bool musicState = GameAudio.IsMusicOn();
        bool sfxState = GameAudio.IsSFXOn();

        musicToggle.SetToggle(musicState);
        sfxToggle.SetToggle(sfxState);

        Sequence settingOn = DOTween.Sequence();
        settingOn.Append(settingPanel.DOAnchorPosY(0, 0.3f, true));
        settingOn.Append(settingPanel.DOJumpAnchorPos(settingPanel.anchoredPosition, 25, 1, 0.5f));
        settingOn.Play();
    }

    public void ToggleMusic()
    {
        bool musicState = GameAudio.IsMusicOn();

        GameAudio.ToggleMusic(!musicState);
        musicToggle.SetToggle(!musicState);
        PlayerPrefs.SetInt("mute_music", Convert.ToInt16(musicState));
    }

    public void ToggleSFX()
    {
        bool sfxState = GameAudio.IsSFXOn();

        GameAudio.ToggleSFX(!sfxState);
        sfxToggle.SetToggle(!sfxState);
        PlayerPrefs.SetInt("mute_sfx", Convert.ToInt16(sfxState));
    }

    public void OpenHome()
    {
        if (GameInstance.GetGameState() == GameState.SETTINGS)
        {
            settingPanel.DOAnchorPosY(settingPanelFirstPosition.y, 0.5f, true);
            GameInstance.SetGameState(GameState.MENU);
            ActivateRotator();
        }
    }

    public void BuyNoAds()
    {
        Purchaser.instance.BuyNoAds();
    }

    public void ShowLeaderboard()
    {
        GameInstance.ShowLeaderBoards();
    }

}

[System.Serializable]
public struct UIImageToggle
{
    public Sprite on;
    public Sprite off;
    public Image imageReference;

    public void SetToggle(bool state)
    {
        if (state)
        {
            imageReference.sprite = on;
        }
        else
        {
            imageReference.sprite = off;
        }
    }
}
