﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LevelGameUI : GameUI
{
    public GameObject gameOverPanel;
    public GameObject gameplayPanel;

    public List<BlackAndWhiteUI> gameplayElement;
    public List<BlackAndWhiteUI> gameOverElement;
    public BlackAndWhiteUI gameOverScore;
    public BlackAndWhiteUI gameOverBackground;
    public BlackAndWhiteUI replayButton;

    public GameObject tutorialPanel;

    public Image tutorialTapLeft;
    public Image tutorialTapRight;
    public Text tutorialInstruction;

    protected override void Awake()
    {
        base.Awake();
        try
        {
            tutorialTapLeft.gameObject.SetActive(false);
            tutorialTapRight.gameObject.SetActive(false);
            tutorialInstruction.gameObject.SetActive(false);
            tutorialPanel.SetActive(false);
        }
        catch
        {

        }
    }

    public void OnUITutorial(string instruction)
    {
        tutorialInstruction.gameObject.SetActive(true);
        tutorialInstruction.text = instruction;
    }

    public void OnUITutorial(bool right, string instruction)
    {
        if (right)
        {
            tutorialTapRight.gameObject.SetActive(true);
            tutorialTapLeft.gameObject.SetActive(false);
        }
        else
        {
            tutorialTapRight.gameObject.SetActive(false);
            tutorialTapLeft.gameObject.SetActive(true);
        }
        tutorialInstruction.gameObject.SetActive(true);
        tutorialInstruction.text = instruction;

    }

    public void OnTutorialComplete()
    {
        tutorialTapLeft.gameObject.SetActive(false);
        tutorialTapRight.gameObject.SetActive(false);
        tutorialInstruction.gameObject.SetActive(false);
        gameplayPanel.SetActive(false);

        tutorialPanel.SetActive(true);
        RectTransform gameOverRect = tutorialPanel.GetComponent<RectTransform>();
        Vector2 firstPost = gameOverRect.anchoredPosition;
        firstPost.y = 1000;
        gameOverRect.anchoredPosition = firstPost;

        Sequence gameOverShow = DOTween.Sequence();
        gameOverShow.Append(gameOverRect.DOAnchorPosY(0, 0.3f));
        gameOverShow.Append(gameOverRect.DOJumpAnchorPos(gameOverRect.anchoredPosition, 25, 1, 0.5f));
        gameOverShow.Play();
    }

    public void OnGameOver()
    {
        gameOverPanel.SetActive(true);
        gameplayPanel.SetActive(false);

        RectTransform gameOverRect = gameOverPanel.GetComponent<RectTransform>();
        Vector2 firstPost = gameOverRect.anchoredPosition;
        firstPost.y = 1000;
        gameOverRect.anchoredPosition = firstPost;

        LevelGameMode gameMode = (LevelGameMode)this.gameMode;
        string score = gameMode.GetScore().ToString();

        Vector2 sizeReplay = replayButton.image.rectTransform.sizeDelta;

        Sequence gameOverShow = DOTween.Sequence();
        gameOverShow.Append(replayButton.image.DOFade(0, 0));
        gameOverShow.Append(gameOverRect.DOAnchorPosY(0, 0.3f));
        gameOverShow.Append(gameOverRect.DOJumpAnchorPos(gameOverRect.anchoredPosition, 25, 1, 0.5f));
        gameOverShow.Append(gameOverScore.text.DOText(score, 2f, true, ScrambleMode.Numerals));
        gameOverShow.Insert(1.7f, replayButton.image.DOFade(1, 0.2f));
        gameOverShow.Insert(1.8f, replayButton.image.rectTransform.DOSizeDelta(new Vector2(sizeReplay.x + 10, sizeReplay.y + 10), 0.2f));
        gameOverShow.Insert(2f, replayButton.image.rectTransform.DOSizeDelta(sizeReplay, 0.1f));
        gameOverShow.Play();

        DotPlayer dotPlayer = (DotPlayer)gameMode.GetPlayer();
        if (!dotPlayer.playerInformation.IsBlack())
        {
            //setup white
            gameOverBackground.ChangeToWhite();
            replayButton.ChangeToBlack();
            gameOverScore.ChangeToBlack();
            foreach (BlackAndWhiteUI element in gameOverElement)
            {
                element.ChangeToBlack();
            }

        }
        else
        {
            //setup black
            gameOverBackground.ChangeToBlack();
            replayButton.ChangeToWhite();
            gameOverScore.ChangeToWhite();
            foreach (BlackAndWhiteUI element in gameOverElement)
            {
                element.ChangeToWhite();
            }
        }
    }

    public void OnScoreChange(int newScore)
    {
        gameplayElement[0].text.text = newScore.ToString();
        Vector3 scaleNow = gameplayElement[0].text.transform.localScale;
        gameplayElement[0].text.transform.DOPunchScale(scaleNow, 0.5f);
    }

    public void OnGamePlay()
    {
        gameOverPanel.SetActive(false);
        gameplayPanel.SetActive(true);
        gameOverScore.text.text = "0";
    }

    public void OnChangeElement()
    {
        foreach (BlackAndWhiteUI uiText in gameplayElement)
        {
            uiText.ChangeElement();
        }
    }

}

[System.Serializable]
public struct BlackAndWhiteUI
{
    public Text text;
    public Image image;

    public void ChangeElement()
    {
        if (text != null)
        {
            if (text.color == Color.white)
            {
                text.color = Color.black;
            }
            else
            {
                text.color = Color.white;
            }
        }
        else
        {
            if (image.color == Color.white)
            {
                image.color = Color.black;
            }
            else
            {
                image.color = Color.white;
            }
        }
    }

    public void ChangeToBlack()
    {
        if (text != null)
        {
            text.color = Color.black;
        }
        else
        {
            image.color = Color.black;
        }
    }

    public void ChangeToWhite()
    {
        if (text != null)
        {
            text.color = Color.white;
        }
        else
        {
            image.color = Color.white;
        }
    }

    public bool IsBlack()
    {
        if (text.color == Color.black)
        {
            return true;
        }
        else
        {
            return true;
        }
    }
}