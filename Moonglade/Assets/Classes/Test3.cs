﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using System.Text.RegularExpressions;

public class Test3 : MonoBehaviour
{
    public List<GridArea> grids;

    private void Awake()
    {
        GetFile();
    }

    public void GetFile()
    {
        int lineCount = 1;
        int currentNumber = 0;
        int currentCaseNumberLine = 0;
        int nextcaseNumberLine = 2;
        int writingCharListIndex = 0;

        string line;
        StreamReader file = new StreamReader(Application.persistentDataPath + "/input.in");
        while ((line = file.ReadLine()) != null)
        {
            if (lineCount == 1)
            {
                int useCase = Int32.Parse(line);

                for (int x = 0; x < useCase; x++)
                {
                    grids.Add(new GridArea());
                }
            }
            else if (lineCount == nextcaseNumberLine)
            {
                nextcaseNumberLine = nextcaseNumberLine + (Int32.Parse(line) + 2);
                currentCaseNumberLine = lineCount;
                currentNumber++;

                grids[currentNumber - 1].grid = new List<Col>();

                int howManyRows = Int32.Parse(line);
                for (int x = 0; x < howManyRows; x++)
                {

                    grids[currentNumber - 1].grid.Add(new Col(1));

                }
            }

            if (lineCount < nextcaseNumberLine && lineCount > currentCaseNumberLine && currentCaseNumberLine != 0)
            {
                if (lineCount == currentCaseNumberLine + 1)
                {

                    int howManyCol = Int32.Parse(line);
                    writingCharListIndex = 0;
                }
                else
                {
                    char[] content = line.ToCharArray();
                    foreach (char c in content)
                    {
                        grids[currentNumber - 1].grid[writingCharListIndex].col.Add(c);
                    }
                    writingCharListIndex++;
                }

            }

            lineCount++;
        }

        file.Close();

        FindSolution();
    }

    private void FindSolution()
    {
        for (int x = 0; x < grids.Count; x++)
        {

            grids[x].DetermineArea();
            //lines[x] = "Case " + (x + 1) + ": " + grids[x].totalCount;
            //result += "Case " + (x + 1) + ": " + grids[x].totalCount + "  ";
        }
    }



}
[Serializable]
public class Area
{
    public List<char> army;
    public List<Int2> coord;

    public Area()
    {
        army = new List<char>();
        coord = new List<Int2>();
    }

    public Area(Int2 coord, char army = ' ')
    {
        this.army = new List<char>();
        this.coord = new List<Int2>();

        if (army != ' ' || army != '.')
        {
            this.army.Add(army);
        }

        this.coord.Add(coord);

    }

    public bool IsExist(int row, int col)
    {
        return coord.Exists(i => i.row == row && i.col == col);
    }

}

[Serializable]
public struct Int2
{
    public int row;
    public int col;

    public Int2(int row, int col)
    {
        this.row = row;
        this.col = col;
    }
}

[Serializable]
public class GridArea
{
    public List<Col> grid;
    [SerializeField]
    public List<Area> area;

    private string pattern = @"^[a-zA-Z\.]+$";

    public void DetermineArea()
    {
        area = new List<Area>();


        for (int x = 0; x < grid.Count; x++)
        {
            for (int y = 0; y < grid[x].col.Count; y++)
            {
                if (Regex.IsMatch(grid[x].col[y].ToString(), pattern))
                {
                    bool exists = false;
                    foreach (Area listedArea in area)
                    {
                        exists = listedArea.IsExist(x, y);
                        if (exists)
                        {
                            break;
                        }
                    }
                    if (!exists)
                    {

                        Area newArea = new Area(new Int2(x, y), grid[x].col[y]);
                        Area fullArea = ExpandArea(x, y, newArea);
                        area.Add(newArea);

                    }

                }
            }
        }
    }

    public Area ExpandArea(int row, int col, Area currentArea)
    {
        //find neighbour

        //find upper left
        if (Regex.IsMatch(grid[row - 1].col[col - 1].ToString(), pattern) && row > 0 && col > 0)
        {

        }

        //find upper
        if (Regex.IsMatch(grid[row - 1].col[col].ToString(), pattern) && row > 0)
        {

        }

        //find upper right
        if (Regex.IsMatch(grid[row - 1].col[col + 1].ToString(), pattern) && row > 0 && col < grid[row - 1].col.Count - 1)
        {

        }

        //find left
        if (Regex.IsMatch(grid[row].col[col - 1].ToString(), pattern) && col > 0)
        {

        }

        //find right
        if (Regex.IsMatch(grid[row].col[col + 1].ToString(), pattern) && col < grid[row].col.Count - 1)
        {

        }

        //find lower left
        if (Regex.IsMatch(grid[row + 1].col[col - 1].ToString(), pattern) && col > 0 && row < grid.Count)
        {

        }

        //find lower
        if (Regex.IsMatch(grid[row + 1].col[col].ToString(), pattern) && row < grid.Count)
        {

        }

        //find lower right
        if (Regex.IsMatch(grid[row + 1].col[col + 1].ToString(), pattern) && col < grid[row + 1].col.Count && row < grid.Count)
        {

        }

        return new Area();
    }
}
